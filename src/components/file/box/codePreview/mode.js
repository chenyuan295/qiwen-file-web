/**
 * 这里引入几个常用的语言解析模式
 * 全量语言模式参考，https://codemirror.net/mode/
 */
import 'codemirror/mode/clike/clike.js' //  C | C++ | Objective-C | Scala | Ceylon | Java 语言
import 'codemirror/mode/javascript/javascript.js' //  JavaScript 语言
import 'codemirror/mode/css/css.js' //  css 语言 | less 预编译器 | scss 预编译器
import 'codemirror/mode/sass/sass.js' //  sass 预编译器
import 'codemirror/mode/stylus/stylus.js' //  stylus 预编译器
import 'codemirror/mode/vue/vue.js' //  vue.js 框架
import 'codemirror/mode/sql/sql.js' //  sql 语言
import 'codemirror/mode/xml/xml.js' //  xml 语言
import 'codemirror/mode/htmlmixed/htmlmixed.js' //  html 标记语言
